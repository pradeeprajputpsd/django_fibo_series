from django.contrib import admin
from .models import Tutorial , TutorialSeries, TutorialCategory, FibonacciResults , AutoCompletesearch
from tinymce.widgets import TinyMCE
from django.db import models
# Register your models here.

class TutorialAdmin(admin.ModelAdmin):  #define admin models fields
	fieldsets = [("Title/date",{"fields":["tutorial_title", "tutorial_published"]}),("Content",{"fields":["tutorial_content"]})]
		# fieldsets used to overloading order of fields and provide titles also rsepect to fields

	formfield_overrides = {
		models.TextField: {'widget': TinyMCE()}
	}	


'''                   
	fields = [ "tutorial_title",            #fields used to overloading order of fields
				"tutorial_published", 
				"tutorial_content"
				]
'''	
    




admin.site.register(Tutorial, TutorialAdmin) #to registor model and can see in admin page after login
admin.site.register(FibonacciResults)
admin.site.register(AutoCompletesearch)








