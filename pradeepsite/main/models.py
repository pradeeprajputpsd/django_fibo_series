from __future__ import unicode_literals

from django.db import models
from datetime import datetime



class TutorialCategory(models.Model):
	tutorial_category = models.CharField(max_length=200)
	category_summary = models.CharField(max_length=200)
	category_slug = models.CharField(max_length=200)

	class Meta:
		verbose_name_plural = "Categories"

	def __str__(self):
		return self.tutorial_category
	



class TutorialSeries(models.Model):
	tutorial_series = models.CharField(max_length=200)
	tutorial_category = models.ForeignKey(TutorialCategory, default=1, verbose_name="Category", on_delete=models.SET_DEFAULT)
	series_summary = models.CharField(max_length=200)

	class Meta:
		verbose_name_plural = "Series"

	def __str__(self):
		return self.tutorial_series



class AutoCompletesearch(models.Model):
	search_string = models.CharField(max_length=200)
	string_frequency = models.IntegerField()
	def __str__(self):
		return self.search_string		
		





# Create your models here.
class Tutorial(models.Model):
	tutorial_title = models.CharField(max_length=200)
	tutorial_content = models.TextField()
	tutorial_published = models.DateTimeField("date published" , default=datetime.now()) #default keyword use to provide value value to a field

	tutorial_series = models.ForeignKey(TutorialSeries, verbose_name="Series", blank=True, null=True , on_delete=models.CASCADE)
	tutorial_slug = models.CharField(max_length=200, default=1)
	
	def __str__(self):
		return self.tutorial_title


class FibonacciResults(models.Model):

    number = models.IntegerField()
    result = models.IntegerField()
    time_taken = models.CharField(max_length=255)

    class Meta:
        db_table = 'fibonacci_results'

    def __unicode__(self):
        return self.number
