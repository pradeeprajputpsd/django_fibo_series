from django.shortcuts import render , redirect
from django.http import HttpResponse , HttpResponseRedirect
from .models import Tutorial , AutoCompletesearch
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout, authenticate
from django.contrib import messages
from .form import NewUserForm

import time

from .models import FibonacciResults
# Create your views here.


'''
def homepage(request):
	return HttpResponse("Wow this is an <strong> Test Web </strong> app")
'''	
'''
#table uotput function
def homepage(request):
	return render(request=request, template_name="home/tableoutput.html", context={"tutorials": Tutorial.objects.all})
'''


def homepage(request):
	return render(request=request, template_name="home/homepage.html", context={"tutorials": Tutorial.objects.all})


def register(request):
	if request.method == "POST":
		form = NewUserForm(request.POST)
		if form.is_valid():
			user = form.save()
			username = form.cleaned_data.get('username')
			messages.success(request, f"New Account created: {username}")
			login(request, user)
			messages.info(request, f"You are now logged in as: {username}")
			return redirect("main:homepage")
		else:
			for msg in form.error_messages:
				print(form.error_messages[msg])
		   	

	form = NewUserForm
	return render(request, "home/register.html", context={"form":form})


def logout_request(request):
	logout(request)
	messages.info(request, "Logged out successfully!")
	return redirect("main:homepage")


def login_request(request):
	if request.method == "POST":
		form = AuthenticationForm(request, data=request.POST)
		if form.is_valid():
			username = form.cleaned_data.get('username')
			password = form.cleaned_data.get('password')
			user = authenticate(username=username, password=password)
			if user is not None:
				login(request, user)
				messages.info(request, f"You are now logged in as {username}")
				return redirect("main:homepage")
			else:
				messages.error(request, "Invalid username or password")
		else:
				messages.error(request, "Invalid username or password")		

	form = AuthenticationForm()
	return render(request, "home/login.html", {"form":form})

def fibonacci_calculation(num):
    if num < 2:
        return 1
    else:
        num_seq_1 = 1
        num_seq_2 = 1
        for i in range(2, num):
            temp = num_seq_1 + num_seq_2
            num_seq_1 = num_seq_2
            num_seq_2 = temp
        return num_seq_2


def fibo(request):
    num = 0
    result = 0
    time_taken = 0

    if request.GET.get('number'):
        start_time = time.time()
        number = request.GET.get('number')
        num = int(number)
        result = fibonacci_calculation(num)
        end_time = time.time() - start_time
        time_taken = str(end_time)[0:4]

        obj = FibonacciResults.objects.create(
            number=num, result=result, time_taken=time_taken)
        obj.save()

    return render(
        request,
        'fibo.html',
        {
            'number': num,
            'result': result,
            'time_taken': time_taken
        }
    )	



def searchmain(request):

	frequency = 0
	frequency1 = ''
	
	obj = AutoCompletesearch.objects.all()
	if obj :
		data = []
		for row in obj:
			row.search_string

	if request.GET.get('search'):
		searchtext = request.GET.get('search')


		obj = AutoCompletesearch.objects.filter(search_string__contains=searchtext)
		#obj = AutoCompletesearch.objects.all()
		if obj :
		 	frequency1 = obj[0].string_frequency
		 	print(frequency1)

	return render(
		request,
		'home/search.html',
		{
			'frequency': obj,
			'output': frequency1,
			
		
		}


	)    	